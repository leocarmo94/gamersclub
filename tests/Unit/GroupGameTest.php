<?php

namespace Tests\Unit;

use App\Entities\Group;
use App\Entities\GroupGame;
use App\Entities\Team;
use App\Entities\Tournament;
use App\Services\GenerateGameResult;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class GroupGameTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * Attributes
     * @var array
     */
    protected $data = [];

    /**
     * @var GroupGame
     */
    protected $group_game;

    /**
     * SetUp
     */
    public function setUp()
    {
        parent::setUp();

        $tournament = factory(Tournament::class)->create(config('tournament.template.default'));

        $group = factory(Group::class)->create([
            'tournament_id' => $tournament->id
        ]);

        $first_team = factory(Team::class)->create();
        $second_team = factory(Team::class)->create();

        $this->data = array_merge(
            [
                'group_id' => $group->id,
                'first_team_id' => $first_team->id,
                'second_team_id' => $second_team->id
            ],
            $this->data
        );

        $this->group_game = GroupGame::create(
            $this->data
        );
    }

    public function testGroupGameCreation()
    {
        $this->assertEquals(
            $this->data,
            $this->group_game->only(
                array_keys($this->data)
            )
        );
    }

    /**
     * @throws \Exception
     */
    public function testGroupGameSimulation()
    {

        $tournament = factory(Tournament::class)->create(config('tournament.template.default'));

        $first_team = factory(Team::class)->create();
        $second_team = factory(Team::class)->create();

        // Generate random game result
        $score = GenerateGameResult::tournament($tournament)
            ->teams($first_team, $second_team)
            ->result();

        // has winner
        $this->assertArrayHasKey('winner', $score);

        // has loser
        $this->assertArrayHasKey('loser', $score);

        // winner score correct
        $this->assertEquals($score['winner']['score'], $tournament->rounds_to_win);

        // loser score less than tournament rounds to win
        $this->assertLessThan($tournament->rounds_to_win, $score['loser']['score']);

        // teams was set correctly?
        $teams_id = [
            $score['winner']['team']->id,
            $score['loser']['team']->id
        ];

        $this->assertContains($first_team->id, $teams_id);
        $this->assertContains($second_team->id, $teams_id);

    }

}
