<?php

namespace Tests\Unit;

use App\Entities\Tournament;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TournamentTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * Attributes
     * @var array
     */
    protected $data = [
        'name' => 'Games Club'
    ];

    /**
     * @var Tournament
     */
    protected $tournament;

    /**
     * SetUp
     */
    public function setUp()
    {
        parent::setUp();

        $this->data = array_merge(config('tournament.template.default'), $this->data);

        $this->tournament = Tournament::create(
            $this->data
        );
    }

    /**
     * Test tournament creation
     */
    public function testTournamentCreation()
    {
        $this->assertEquals(
            $this->data,
            $this->tournament->only(
                array_keys($this->data)
            )
        );
    }

}
