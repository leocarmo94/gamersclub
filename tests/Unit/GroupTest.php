<?php

namespace Tests\Unit;

use App\Entities\Group;
use App\Entities\Team;
use App\Entities\Tournament;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class GroupTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * Attributes
     * @var array
     */
    protected $data = [
        'name' => 'Group A'
    ];

    /**
     * @var Group
     */
    protected $group;

    /**
     * SetUp
     */
    public function setUp()
    {
        parent::setUp();

        $tournament = factory(Tournament::class)->create(config('tournament.template.default'));

        $this->data = array_merge(['tournament_id' => $tournament->id], $this->data);

        $this->group = Group::create(
            $this->data
        );
    }

    public function testGroupCreation()
    {
        $this->assertEquals(
            $this->data,
            $this->group->only(
                array_keys($this->data)
            )
        );
    }

    public function testTeamGroupAttach()
    {
        $teams = factory(Team::class, 5)->create();

        $this->group->teams()->attach($teams);

        $this->assertCount(
            5,
            $this->group->teams()->get()
        );
    }



}
