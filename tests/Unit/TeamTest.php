<?php

namespace Tests\Unit;

use App\Entities\Team;
use App\Entities\Tournament;
use App\Repositories\TeamRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TeamTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * Attributes
     * @var array
     */
    protected $data = [
        'name' => 'Games Club Team',
        'image' => 'image.jpg'
    ];

    /**
     * @var Team
     */
    protected $team;

    /**
     * SetUp
     */
    public function setUp()
    {
        parent::setUp();

        $this->team = Team::create(
            $this->data
        );
    }

    /**
     * Test team creation
     */
    public function testTeamCreation()
    {
        $this->assertEquals(
            $this->data,
            $this->team->only(
                array_keys($this->data)
            )
        );
    }

    public function testTeamTournamentSubscription()
    {
        $tournament = factory(Tournament::class)->create(config('tournament.template.default'));

        $this->team->tournaments()->attach($tournament);

        $this->assertCount(
            1,
            $tournament->teams()->get()
        );
    }

    public function testGetLimitedTeams()
    {
        factory(Team::class, 10)->create();

        $teams = (new TeamRepository)->getWithLimit(5);

        $this->assertCount(
            5,
            $teams
        );
    }

}
