# Simulação de torneio

Esta simulação será feita em duas etapas: 

##### Fase de grupos
Os times serão dividos em grupos e todos jogarão contra todos, os vecedores se classificam para a próxima fase.
O critério de desempate é a quantidade de pontos (vitórias) e numeros de rounds vencidos em cada jogo.

##### Playoffs
Os vencedores irão se enfrentar em um sistema de eliminação simples (Bracket)

## Iniciando
Para iniciar o sistema é preciso instalar todas as dependencias do composer:
```sh
composer install
```
Realizar a cópia do .env.example e configurar a conexão do banco de dados
```sh
cp .env.example .env
```
Gerar a key para o Laravel:
```sh
php artisan key:generate
```
Migrar a database com as seeds:
```sh
php artisan migrate --seed
```
Criar o arquivo `database_test.sqlite` para a database de testes dentro da pasta `database` e realizar a migração:

```sh
php artisan migrate --database=sqlite_test
```

## Testando
Escrevi alguns testes unitários:
```sh
vendor/bin/phpunit
```
*Infelizmente não consegui realizar todos que gostaria por conta do tempo...*

## Torneio
Todo o torneio pode ser manipulado pelo `artisan` através do comando:
```sh
php artisan tournament
```
- É possível personalizar as diretrizes do torneio através do arquivo de configuração `tournament`
- Depois de criar um torneio, automaticamente você será levado passo a passo até o final

#### Criando o torneio
Para criar um novo torneio utilize o seguinte comando:
```sh
php artisan tournament:create
```
Para criar um torneio personalizado, crie as diretrizes dentro do arquivo de configuração e utilize o seguinte paramentro:
```sh
php artisan tournament --template=another_template
```

#### Gerando grupos de um torneio
Depois de criar um torneio você poderá sortear os grupos de times com o seguinte comando:
```sh
php artisan tournament:groups --tournament=TOURNAMENT_ID
```
*Caso o TOURNAMENT_ID sejá inválido, será solicitado um novo ID válido*

#### Jogos da fase de grupos
Com os grupos formados, é hora de realizar os jogos para as classificatórios para a fase de playoffs:
```sh
php artisan tournament:groups-games --tournament=TOURNAMENT_ID
```

### Playoffs
Com os times classificados, é hora do mata-mata para conhecer-mos o grande campeão:
```sh
php artisan tournament:playoffs --tournament=TOURNAMENT_ID
```

## Créditos
- [Leonardo do Carmo](https://github.com/leocarmo)

