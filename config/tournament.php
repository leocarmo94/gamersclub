<?php

return [

    'template' => [

        'default' => [
            'max_teams' => 80,
            'teams_per_group' => 5,
            'teams_classified_per_group' => 2,
            'rounds_to_win' => 16
        ]

    ]


];
