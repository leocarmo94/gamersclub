<?php
namespace App\Repositories;

use App\Entities\GroupTeam;

class GroupTeamRepository
{

    /**
     * @param int $tournament_id
     * @return \Illuminate\Support\Collection
     */
    public function getByTournamentIdGroupStageOrderByClassification(int $tournament_id)
    {
        return GroupTeam::with(['team'])
            ->whereHas('group', function($query)use($tournament_id) {
                $query->where('tournament_id',  $tournament_id);
            })
            ->orderBy('group_id')
            ->orderBy('team_points', 'DESC')
            ->orderBy('team_rounds_won', 'DESC')
            ->get();
    }


}