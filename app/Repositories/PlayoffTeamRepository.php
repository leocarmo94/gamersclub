<?php
namespace App\Repositories;

use App\Entities\PlayoffTeam;

class PlayoffTeamRepository
{

    /**
     * @param int $tournament_id
     * @return \Illuminate\Support\Collection
     */
    public function getAllByTournamentId(int $tournament_id)
    {
        return PlayoffTeam::with(['team'])->where('tournament_id', $tournament_id)->get();
    }


}