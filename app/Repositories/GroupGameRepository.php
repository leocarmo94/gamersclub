<?php
namespace App\Repositories;

use App\Entities\GroupGame;

class GroupGameRepository
{

    /**
     * @param int $tournament_id
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getByTournamentId(int $tournament_id)
    {

        return GroupGame::with(['group', 'firstTeam', 'secondTeam'])
            ->whereHas('group', function($query)use($tournament_id) {
                $query->where('tournament_id',  $tournament_id);
            })
            ->get();

    }

    /**
     * @param int $tournament_id
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getPendingGamesByTournamentId(int $tournament_id)
    {

        return GroupGame::with(['group', 'firstTeam', 'secondTeam'])
            ->whereHas('group', function($query)use($tournament_id) {
                $query->where('tournament_id',  $tournament_id);
            })
            ->whereNull('winner_team_id')
            ->get();

    }


}