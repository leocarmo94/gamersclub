<?php
namespace App\Repositories;

use App\Entities\Team;

class TeamRepository
{

    /**
     * @param int $limit
     * @return \Illuminate\Support\Collection
     */
    public function getWithLimit(int $limit)
    {
        return Team::limit($limit)->inRandomOrder()->get();
    }


}