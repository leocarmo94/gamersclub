<?php
namespace App\Repositories;

use App\Entities\Group;

class GroupRepository
{

    /**
     * @param int $tournament_id
     * @return \Illuminate\Support\Collection
     */
    public function getByTournamentId(int $tournament_id)
    {
        return Group::where('tournament_id', $tournament_id)->get();
    }

    /**
     * @param int $tournament_id
     * @return \Illuminate\Support\Collection
     */
    public function getByTournamentIdWithRelations(int $tournament_id)
    {
        return Group::with(['teams'])->where('tournament_id', $tournament_id)->get();
    }

}