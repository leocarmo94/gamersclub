<?php
namespace App\Repositories;

use App\Entities\PlayoffGame;

class PlayoffGameRepository
{

    /**
     * @param int $tournament_id
     * @return \Illuminate\Support\Collection
     */
    public function getAllByTournamentId(int $tournament_id)
    {
        return PlayoffGame::where('tournament_id', $tournament_id)->get();
    }


}