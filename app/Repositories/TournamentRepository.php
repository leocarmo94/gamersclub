<?php
namespace App\Repositories;

use App\Entities\Tournament;

class TournamentRepository
{

    /**
     * @param int $id
     * @return Tournament
     */
    public function find(int $id)
    {
        return Tournament::find($id);
    }


}