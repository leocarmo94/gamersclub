<?php
namespace App\Repositories;

use App\Entities\Group;
use App\Entities\TeamClassification;

class TeamClassificationRepository
{

    /**
     * @param int $tournament_id
     * @return \Illuminate\Support\Collection
     */
    public function getByTournamentId(int $tournament_id)
    {
        return TeamClassification::with(['team'])
            ->where('tournament_id', $tournament_id)
            ->orderBy('classification')
            ->get();
    }

}