<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class GroupTeam extends Model
{

    protected $table = 'group_team';

    protected $fillable = [
        'team_id', 'group_id', 'team_points', 'team_rounds'
    ];

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

}
