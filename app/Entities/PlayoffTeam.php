<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PlayoffTeam extends Model
{

    protected $fillable = [
        'tournament_id', 'team_id'
    ];

    public function tournament()
    {
        return $this->belongsTo(Tournament::class, 'tournament_id');
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

}
