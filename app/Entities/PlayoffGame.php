<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PlayoffGame extends Model
{

    protected $fillable = [
        'tournament_id', 'round', 'winner_team_id', 'loser_team_id', 'winner_rounds', 'loser_rounds'
    ];

}
