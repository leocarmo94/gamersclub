<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    protected $fillable = [
        'name', 'max_teams', 'teams_per_group', 'teams_classified_per_group', 'rounds_to_win'
    ];

    /*
     *  ### Relations
     */

    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }

}
