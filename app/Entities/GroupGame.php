<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class GroupGame extends Model
{
    protected $fillable = [
        'group_id', 'first_team_id', 'second_team_id',
        'winner_team_id', 'loser_team_id', 'winner_rounds', 'loser_rounds'
    ];

    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }

    public function firstTeam()
    {
        return $this->belongsTo(Team::class, 'first_team_id');
    }

    public function secondTeam()
    {
        return $this->belongsTo(Team::class, 'second_team_id');
    }
}
