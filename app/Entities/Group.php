<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'name', 'tournament_id'
    ];

    /*
     *  ### Relations
     */

    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }

    public function tournament()
    {
        return $this->belongsTo(Tournament::class, 'tournament_id');
    }

}
