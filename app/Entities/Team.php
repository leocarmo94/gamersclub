<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        'name', 'image'
    ];

    /*
     *  ### Relations
     */

    public function tournaments()
    {
        return $this->belongsToMany(Tournament::class);
    }

    /*
     *  ### Relations
     */

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }

}
