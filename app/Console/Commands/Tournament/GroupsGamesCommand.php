<?php

namespace App\Console\Commands\Tournament;

use App\Entities\GroupGame;
use App\Entities\GroupTeam;
use App\Entities\PlayoffTeam;
use App\Repositories\GroupGameRepository;
use App\Repositories\GroupTeamRepository;
use App\Services\DefineTournamentForConsole;
use App\Services\GenerateGameResult;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GroupsGamesCommand extends Command
{

    use DefineTournamentForConsole;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tournament:groups-games {--tournament=0 : Tournament ID for groups games}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Teams will fight in groups stage and will be classified to playoffs';

    /**
     * @var \App\Entities\Tournament
     */
    protected $tournament;

    /**
     * CreateCommand a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->tournament = $this->defineTournament(
            $this->option('tournament')
        );

        // Get all pending games in the tournament
        $pending_games = (new GroupGameRepository())->getPendingGamesByTournamentId($this->tournament->id);

        // Check quantity
        if($pending_games->count() === 0) {
            return $this->error('Groups in this tournament have already fought. Go for playoffs!');
        }

        // Just for visual proposes
        $this->comment('### The teams will face each other face to face!');

        // Now for each game we will generate the result
        $pending_games->each(function($game) {

            // Just for visual proposes
            $this->info('# Team ' . $game->firstTeam->name . ' vs. ' . $game->secondTeam->name);

            // Generate random game result
            $game_score = GenerateGameResult::tournament($this->tournament)
                ->teams($game->firstTeam, $game->secondTeam)
                ->result();

            // Save results and score
            GroupGame::where('id', $game->id)
                ->update([
                    'winner_team_id' => $game_score['winner']['team']->id,
                    'loser_team_id' => $game_score['loser']['team']->id,
                    'winner_rounds' => $game_score['winner']['score'],
                    'loser_rounds' => $game_score['loser']['score']
                ]);

            // Search groups teams for increment points
            $winner_group_team = GroupTeam::where('group_id', $game->group_id)
                ->where('team_id', $game_score['winner']['team']->id);

            // Increment point and rounds
            $winner_group_team->increment('team_points');
            $winner_group_team->increment('team_rounds_won', $game_score['winner']['score']);

            // Increment round for loser
            GroupTeam::where('group_id', $game->group_id)
                ->where('team_id', $game_score['loser']['team']->id)
                ->increment('team_rounds_won', $game_score['loser']['score']);

            // Just for visual proposes
            $this->line('- Result: [' . $game_score['winner']['score'] . '] ' . $game_score['winner']['team']->name . ' vs. [' . $game_score['loser']['score'] . '] ' . $game_score['loser']['team']->name . PHP_EOL);

        });

        // Select teams for playoffs
        $this->teamsForPlayoffs();

        // Just for visual proposes
        $this->comment(PHP_EOL . '### Groups games end! Go for playoffs!');

        // Next step?
        if ($this->confirm('Do you wish to continue to playoffs?')) {

            $this->call('tournament:playoffs', [
                '--tournament' => $this->tournament->id
            ]);

        }

    }

    /**
     * Select winners teams for playoffs
     */
    private function teamsForPlayoffs()
    {

        $this->comment(PHP_EOL . '### Teams for playoffs:');

        PlayoffTeam::insert(
            (new GroupTeamRepository)
            ->getByTournamentIdGroupStageOrderByClassification($this->tournament->id)
            ->chunk($this->tournament->teams_per_group)->map(function($group) {

                return $group->slice(0, $this->tournament->teams_classified_per_group);

            })
            ->flatten()
            ->map(function($group) {

                $this->line('Team ' . $group->team->name . ' classified to playoffs!');

                return [
                    'tournament_id' => $this->tournament->id,
                    'team_id' => $group->team_id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];

            })
            ->toArray()
        );

    }

}
