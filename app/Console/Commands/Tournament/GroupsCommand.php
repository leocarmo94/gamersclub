<?php

namespace App\Console\Commands\Tournament;

use App\Entities\Group;
use App\Entities\GroupGame;
use App\Repositories\GroupRepository;
use App\Services\DefineTournamentForConsole;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GroupsCommand extends Command
{

    use DefineTournamentForConsole;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tournament:groups {--tournament=0 : Tournament ID for sort groups stage}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sort groups stage for a tournament';

    /**
     * CreateCommand a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // Get valid tournament
        $tournament = $this->defineTournament(
            $this->option('tournament')
        );

        // Check if groups was already sorted
        if((new GroupRepository)->getByTournamentId($tournament->id)->count() > 0) {
            return $this->error('Groups in this tournament already sorted! Go for groups games.');
        }

        // Just for visual proposes
        $this->comment('### Groups will be sorted for tournament ' . $tournament->name);

        // Here we chunk groups and sync teams on them
        $tournament
            ->teams()
            ->get()
            ->shuffle()
            ->chunk($tournament->teams_per_group)
            ->each(function($teams)use($tournament){

                // Fake group
                $group = factory(Group::class)->create([
                    'tournament_id' => $tournament->id
                ]);

                // Just for visual proposes
                $this->info(PHP_EOL . 'Group ' . $group->name . ' created with teams:');

                // Just for visual proposes
                $teams->each(function($team) {
                    $this->line($team->name);
                });

                // Sync teams to groups
                $group->teams()->attach($teams);

            });

        // Collection with games to mass insert
        $games = collect();

        // Now, we will generate all games in these groups
        (new GroupRepository)->getByTournamentIdWithRelations($tournament->id)->each(function($group) use ($games) {

            // For each team in this group we will match the opponent
            $group->teams->each(function($team, $index) use ($group, $games) {

                // The last one has already match with all
                if($index === $group->teams->count() - 1) {
                    return false;
                }

                // Here we will match all teams with subsequents in the group
                for ($i = $index + 1; $i < $group->teams->count(); $i++) {

                    $games->push([
                        'group_id' => $group->id,
                        'first_team_id' => $team->id,
                        'second_team_id' => $group->teams->get($i)->id,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);

                }

            }); // end team loop

        }); // end group loop

        // Mass insert
        GroupGame::insert($games->toArray());

        // Just for visual proposes
        $this->comment(PHP_EOL . 'Groups ready! Go for groups games, let\'s fight!.');

        // Next step?
        if ($this->confirm('Do you wish to continue to groups games?')) {

            $this->call('tournament:groups-games', [
                '--tournament' => $tournament->id
            ]);

        }

    }

}
