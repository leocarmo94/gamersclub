<?php

namespace App\Console\Commands\Tournament;

use App\Entities\Tournament;
use App\Repositories\TeamRepository;
use Illuminate\Console\Command;

class CreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tournament:create {--template=default : Tournament template, you can change this in config.tournament}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new tournament and subscribe teams!';

    /**
     * CreateCommand a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // CreateCommand tournament
        $tournament = factory(Tournament::class)->create(
            $this->tournamentConfigs()
        );

        // Just for visual proposes
        $this->info('Tournament ' . $tournament->name . ' created!');

        // Get teams for tournament
        $teams = (new TeamRepository)->getWithLimit($tournament->max_teams);

        // Just for visual proposes
        $this->line('Teams in tournament:');

        // Table of participants teams
        $headers = ['Team Name', 'Team Image'];

        $teams_for_table = $teams->map(function($team) {

           return [
               'name' => $team->name,
               'image' => $team->image
           ];

        });

        // Draw table
        $this->table($headers, $teams_for_table);

        // Sync teams to created tournament
        $tournament->teams()->attach($teams);

        // Just for visual proposes
        $this->info('Tournament ready! Go for groups stage!');

        // Next step?
        if ($this->confirm('Do you wish to continue to groups stage?')) {

            $this->call('tournament:groups', [
                '--tournament' => $tournament->id
            ]);

        }

    }

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    protected function tournamentConfigs()
    {
        $template = $this->option('template');

        if(array_key_exists($template, config('tournament.template'))) {

            return config('tournament.template.' . $template);

        }

        return config('tournament.template.default');
    }

}
