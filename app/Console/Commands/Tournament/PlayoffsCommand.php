<?php

namespace App\Console\Commands\Tournament;

use App\Entities\PlayoffGame;
use App\Entities\TeamClassification;
use App\Repositories\PlayoffGameRepository;
use App\Repositories\PlayoffTeamRepository;
use App\Repositories\TeamClassificationRepository;
use App\Services\DefineTournamentForConsole;
use App\Services\GenerateGameResult;
use Carbon\Carbon;
use Illuminate\Console\Command;

class PlayoffsCommand extends Command
{

    use DefineTournamentForConsole;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tournament:playoffs {--tournament=0 : Tournament ID for playoffs}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Winners from group stage will fight face to face!';

    /**
     * @var \App\Entities\Tournament
     */
    protected $tournament;

    /**
     * @var int
     */
    protected $round = 1;

    /**
     * CreateCommand a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // Get valid tournament
        $this->tournament = $this->defineTournament(
            $this->option('tournament')
        );

        // Check if games have already happened
        if((new PlayoffGameRepository)->getAllByTournamentId($this->tournament->id)->count() > 0) {
            return $this->error('Playoffs of this tournament have already happened!');
        }

        // Get all teams from this tournament and shuffle them to fight
        $teams = (new PlayoffTeamRepository)->getAllByTournamentId($this->tournament->id)->shuffle()->keyBy('team_id');

        // Start the classification collection
        $tournament_classification = collect();

        // Start the games collection
        $playoffs_games = collect();

        // While we have teams, they will fight!
        while ($teams->count() > 1) {

            // Just for visual proposes
            $this->comment(PHP_EOL . '### Round ' . $this->round . ' fight!');

            // Chunk teams in pairs
            $teams->chunk(2)->each(function($fight) use ($teams, $tournament_classification, $playoffs_games) {

                // Generate a random score
                $game_score = GenerateGameResult::tournament($this->tournament)
                    ->teams($fight->first()->team, $fight->last()->team)
                    ->result();

                // Just for visual proposes
                $this->line('- Result: [' . $game_score['winner']['score'] . '] ' . $game_score['winner']['team']->name . ' vs. [' . $game_score['loser']['score'] . '] ' . $game_score['loser']['team']->name . PHP_EOL);

                // Remove the loser from teams
                $teams->forget($game_score['loser']['team']->id);

                // Push the loser to classification
                $tournament_classification->push($game_score['loser']['team']->id);

                // Push game to collection
                $playoffs_games->push([
                    'tournament_id' => $this->tournament->id,
                    'round' => $this->round,
                    'winner_team_id' => $game_score['winner']['team']->id,
                    'loser_team_id' => $game_score['loser']['team']->id,
                    'winner_rounds' => $game_score['winner']['score'],
                    'loser_rounds' => $game_score['loser']['score'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

            }); // end round

            // Next round
            $this->round++;

        }

        // Save all games
        PlayoffGame::insert($playoffs_games->toArray());

        // Push winner to classification
        $tournament_classification->push($teams->first()->team->id);

        // Just for visual proposes
        $this->info('Winner: ' . $teams->first()->team->name);

        // Reverse, reset keys and save final classification in this tournament
        $teams_classifications = $tournament_classification->reverse()->values()->map(function($team_id, $key) {

            return [
                'tournament_id' => $this->tournament->id,
                'classification' => $key + 1,
                'team_id' => $team_id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];

        });

        // Mass insert
        TeamClassification::insert($teams_classifications->toArray());

        // Just for visual proposes
        $this->comment(PHP_EOL . 'That\'s all folks, we hope you enjoyed!');

        // Table headers
        $headers = ['Classification', 'Team'];

        // Table data
        $classification = (new TeamClassificationRepository)
            ->getByTournamentId($this->tournament->id)
            ->map(function ($classification){

                return [
                    'Classification' => $classification->classification,
                    'Team' => $classification->team->name
                ];

            });

        $this->table($headers, $classification);

    }

}
