<?php
namespace App\Services;

use App\Repositories\TournamentRepository;

trait DefineTournamentForConsole
{

    /**
     * @param $tournamentId
     * @return \App\Entities\Tournament
     */
    protected function defineTournament($tournamentId)
    {

        // Find the tournament
        $tournament = (new TournamentRepository)->find((int) $tournamentId);

        // Tournament is valid?
        if($tournamentId === null || $tournament === null) {

            // Ask for a valid tournament
            $tournamentId = $this->ask('Invalid Tournament, please enter a valid ID');

            // Let's check again
            return $this->defineTournament($tournamentId);

        }

        // All right
        return $tournament;

    }

}