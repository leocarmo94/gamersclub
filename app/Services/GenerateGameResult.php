<?php
namespace App\Services;

use App\Entities\Team;
use App\Entities\Tournament;

class GenerateGameResult
{

    /**
     * @var Tournament
     */
    private $tournament;

    /**
     * @var Team
     */
    private $first_team;

    /**
     * @var Team
     */
    private $second_team;

    private $score = [
        'team_one' => 0,
        'team_two' => 0
    ];

    private function __construct(Tournament $tournament)
    {
        $this->tournament = $tournament;
    }

    /**
     * @param Tournament $tournament
     * @return GenerateGameResult
     */
    public static function tournament(Tournament $tournament)
    {
        return new self($tournament);
    }

    public function teams(Team $first_team, Team $second_team)
    {
        $this->first_team = $first_team;
        $this->second_team = $second_team;

        return $this;
    }

    /**
     * Return array with score for team_one and team_two
     * @return array
     * @throws \Exception
     */
    public function result()
    {

        if($this->first_team === null || $this->second_team === null) {
            throw new \Exception('Teams was not defined correctly!');
        }

        // Collection for random select a round winner
        $teams = collect(['team_one', 'team_two']);

        // Loop while a team reach tournament rounds to victory
        while($this->score['team_one'] < $this->tournament->rounds_to_win && $this->score['team_two'] < $this->tournament->rounds_to_win) {

            // Sort a random winner for actual round
            $this->score[$teams->random()]++;

        }

        // Check the winner
        if($this->score['team_one'] > $this->score['team_two']) {

            // Team on won
            return [
                'winner' => [
                    'team' => $this->first_team,
                    'score' => $this->score['team_one']
                ],
                'loser' => [
                    'team' => $this->second_team,
                    'score' => $this->score['team_two']
                ]
            ];

        }

        // Team two won
        return [
            'winner' => [
                'team' => $this->second_team,
                'score' => $this->score['team_two']
            ],
            'loser' => [
                'team' => $this->first_team,
                'score' => $this->score['team_one']
            ]
        ];

    }

}