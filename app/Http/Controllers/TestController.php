<?php

namespace App\Http\Controllers;

use App\Entities\GroupGame;
use App\Entities\GroupGameResult;
use App\Entities\GroupTeam;
use App\Entities\PlayoffGame;
use App\Entities\PlayoffTeam;
use App\Entities\TeamClassification;
use App\Entities\Tournament;
use App\Repositories\GroupGameRepository;
use App\Repositories\GroupTeamRepository;
use App\Repositories\PlayoffTeamRepository;
use App\Repositories\TeamClassificationRepository;
use App\Services\GenerateGameResult;

class TestController extends Controller
{
    public function testMain()
    {

        /*
        $tournament = Tournament::find(1);

        $teams = (new PlayoffTeamRepository)->getAllByTournamentId($tournament->id)->shuffle()->keyBy('team_id');

        $tournament_classification = collect();

        $round = 1;

        echo '<pre>';

        while ($teams->count() > 1) {

            echo PHP_EOL . 'Start round #' . $round . PHP_EOL;

            $teams->chunk(2)->each(function ($fight)use($tournament, $teams, $round, $tournament_classification) {

                $score = GenerateGameResult::start($tournament)->result();

                // Check the winner
                if($score['team_one'] > $score['team_two']) {

                    // Create results data
                    $result = [
                        'winner' => [
                            'id' => $fight->first()->team_id,
                            'score' => $score['team_one']
                        ],
                        'loser' => [
                            'id' => $fight->last()->team_id,
                            'score' => $score['team_two']
                        ]
                    ];

                    // Team two won
                } else {

                    // Create results data
                    $result = [
                        'winner' => [
                            'id' => $fight->last()->team_id,
                            'score' => $score['team_two']
                        ],
                        'loser' => [
                            'id' => $fight->first()->team_id,
                            'score' => $score['team_one']
                        ]
                    ];

                }

                echo '- Result: [' . $score['team_one'] . '] ' . $fight->first()->team->id . ' vs. [' . $score['team_two'] . '] ' . $fight->last()->team->id . PHP_EOL;

                PlayoffGame::create([
                    'tournament_id' => $tournament->id,
                    'round' => $round,
                    'winner_team_id' => $result['winner']['id'],
                    'loser_team_id' => $result['loser']['id'],
                    'winner_rounds' => $result['winner']['score'],
                    'loser_rounds' => $result['loser']['score']
                ]);

                $teams->forget($result['loser']['id']);

                $tournament_classification->push($result['loser']['id']);

            });


            $round++;

        }

        $tournament_classification->push($teams->first()->team->id);

        echo PHP_EOL . 'Winner: ' . $teams->first()->team->id;

        $class = $tournament_classification->reverse()->values()->map(function($team_id, $key)use($tournament) {

            return [
                'tournament_id' => $tournament->id,
                'classification' => $key + 1,
                'team_id' => $team_id
            ];

        })->toArray();

        dd($class);

        TeamClassification::create($class);

        dd($tournament_classification->reverse()->values());

        */
        echo 'hello';

    }
}
