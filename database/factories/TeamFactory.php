<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Team::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'image' => $faker->imageUrl()
    ];
});
