<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Group::class, function (Faker $faker) {
    return [
        'name' => $faker->countryCode . $faker->randomNumber(2)
    ];
});
