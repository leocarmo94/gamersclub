<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayoffGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playoff_games', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('tournament_id')->unsigned();
            $table->foreign('tournament_id')->references('id')->on('tournaments')->onDelete('cascade');

            $table->integer('round');

            $table->integer('winner_team_id')->unsigned()->nullable();
            $table->foreign('winner_team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->integer('loser_team_id')->unsigned()->nullable();
            $table->foreign('loser_team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->integer('winner_rounds')->nullable();
            $table->integer('loser_rounds')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playoff_games');
    }
}
