<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_games', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');

            $table->integer('first_team_id')->unsigned();
            $table->foreign('first_team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->integer('second_team_id')->unsigned();
            $table->foreign('second_team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->integer('winner_team_id')->unsigned()->nullable();
            $table->foreign('winner_team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->integer('loser_team_id')->unsigned()->nullable();
            $table->foreign('loser_team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->integer('winner_rounds')->nullable();
            $table->integer('loser_rounds')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_games');
    }
}
